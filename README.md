# Media Trooper Digitalagentur


In Zeiten der Digitalisierung von Marketing und Vertrieb ist es besonders wichtig, einen erfahrenen Digital-Spezialist zu haben. Wir als Digitalagentur wissen genau, wo wir Online, Mobil, in der Cloud und in VR ansetzen, um Ihren Brand, Marketing sowie Vertrieb zu digitalisieren:
https://www.mediatrooper.de/digital-spezialist

Wir bei Media Trooper legen viel Wert auf Inhouse-Kompetenz. Nur so bekommen wir die bestmöglichsten Ergebnisse für unsere Kunden in straffen Timings der digitalen Welt gestemmt.

Du solltest Dich bewerben, wenn Dir PHP und MySQL nicht fremd ist und lernen möchtest, wie das in der Cloud funktioniert. Keine Angst, wir beißen nicht. Für das aktuelle Ausbildungsjahr oder ab sofort bieten wir Dir eine Ausbildungsstelle zum FachinformatikerIn Anwendungsentwicklung (m/w) in 65510 Idstein. 

Vergiss bitte nicht, uns von Deinem Talent zu überzeugen.

Jetzt bewerben: https://www.mediatrooper.de/jobs-ausbildung-fachinformatiker

Und natürlich suchen wir auch Webentwickler ;-). https://www.mediatrooper.de/jobs